#ifndef BaseIconNames_h
#define BaseIconNames_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include <QString>

//* namespace for base icons static name wrapper
namespace IconNames
{

    static const QString AboutQt = "qt-logo";
    static const QString Add = "list-add";
    static const QString Color = "format-fill-color";
    static const QString SelectColor = "color-picker";
    static const QString Configure = "configure";
    static const QString Copy = "edit-copy";
    static const QString Cut = "edit-cut";
    static const QString Delete = "user-trash";
    static const QString DialogCancel = "dialog-cancel";
    static const QString DialogClose = "dialog-close";
    static const QString DialogOk = "dialog-ok";
    static const QString DialogAccept = "dialog-ok-apply";
    static const QString Edit = "document-edit";
    static const QString EditClear = "edit-clear-locationbar-rtl";
    static const QString Exit = "application-exit";
    static const QString Find = "edit-find";
    static const QString FindPrevious = "arrow-up-double";
    static const QString FindNext = "arrow-down-double";
    static const QString SelectFont = "preferences-desktop-font";
    static const QString Help = "help-contents";
    static const QString Lock = "document-encrypt";
    static const QString Open = "document-open";
    static const QString OpenFolder = "document-open-folder";
    static const QString Paste = "edit-paste";
    static const QString Print = "document-print";
    static const QString PrintPreview = "document-print-preview";
    static const QString Redo = "edit-redo";
    static const QString Reload = "view-refresh";
    static const QString Remove = "list-remove";
    static const QString Rename = "edit-rename";
    static const QString Revert = "document-revert";
    static const QString Run = "system-run";
    static const QString ShowMenu = "show-menu";
    static const QString Undo = "edit-undo";

    static const QString Information = "help-about";
    static const QString DialogInformation = "dialog-information";
    static const QString DialogWarning = "dialog-warning";

    static const QString Previous = "arrow-left";
    static const QString Next = "arrow-right";
    static const QString Html = "text-html";
    static const QString SymbolicLink = "emblem-symbolic-link";

    // configuration icons
    static const QString PreferencesGeneral = "system-run";
    static const QString PreferencesNavigation = "input-mouse";
    static const QString PreferencesColors = "preferences-desktop-color";
    static const QString PreferencesLists = "view-list-tree";
    static const QString PreferencesMultipleViews = "view-split-left-right";
    static const QString PreferencesRecentFiles = "document-open-recent";
    static const QString PreferencesFileAssociations = "preferences-desktop-filetype-association";
    static const QString PreferencesEdition = "edit-rename";
    static const QString PreferencesAppearance = "preferences-desktop-theme";
    static const QString PreferencesTransparency = "preferences-desktop-theme";
    static const QString PreferencesUnsorted = "preferences-other";
    static const QString PreferencesPrinting = "document-print";
    static const QString PreferencesNotifications = "preferences-desktop-notification";
    static const QString PreferencesMainWindow = "preferences-system-windows-actions";
    static const QString PreferencesBackground = "preferences-desktop-wallpaper";
    static const QString PreferencesBookMarks = "bookmarks";
    static const QString PreferencesServer = "network-server";
    static const QString PreferencesCache = "preferences-web-browser-cache";

};

#endif
