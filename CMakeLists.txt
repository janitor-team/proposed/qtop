# $Id$
cmake_minimum_required(VERSION 2.8.12)

project(Top)
set(TOP_VERSION_MAJOR 2)
SET(TOP_VERSION_MINOR 3)
SET(TOP_VERSION_PATCH 4)
set(TOP_VERSION ${TOP_VERSION_MAJOR}.${TOP_VERSION_MINOR}.${TOP_VERSION_PATCH})

########### modules #################
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/base-cmake")
include(Macros)
setup_cmake_settings()
setup_compiler_flags()

########### packages ###############
set(CPACK_PACKAGE_NAME "Top")
set(CPACK_PACKAGE_VENDOR "hugo.pereira@free.fr")
set(CPACK_PACKAGE_VERSION "${TOP_VERSION}")
set(CPACK_SOURCE_GENERATOR TGZ)
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")
include(CPack)

########### configuration files ###############
configure_file(Top.spec.cmake ${CMAKE_SOURCE_DIR}/Top.spec)

########### subdirectories ###############
if(ENABLE_SHARED)

  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
  link_directories(${CMAKE_INSTALL_PREFIX}/lib)

else()

  add_subdirectory(base)
  add_subdirectory(base-qt)
  add_subdirectory(base-server)

endif()

add_subdirectory(src)
