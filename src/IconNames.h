#ifndef IconNames_h
#define IconNames_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "BaseIconNames.h"
#include <QString>

//* namespace for icon static name wrappers
namespace IconNames
{

    static const QString AddTab = "tab-new";
    static const QString RemoveTab = "tab-close";
    static const QString Record = "edit-find";
    static const QString Resume = "media-playback-start";
    static const QString Pause = "media-playback-pause";
    static const QString Kill = "process-stop";
    static const QString Tree = "view-list-tree";
    static const QString PreferencesJobTracking = "help-about";

};

#endif
