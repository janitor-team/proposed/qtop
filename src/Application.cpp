/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Application.h"
#include "BaseStatusBar.h"
#include "ConfigurationDialog.h"
#include "JobInterface.h"
#include "MainWindow.h"
#include "QtUtil.h"
#include "UserSelectionFrame.h"
#include "Util.h"

#include <QApplication>

//____________________________________________
Application::Application( CommandLineArguments arguments ) :
    BaseApplication( nullptr, arguments ),
    Counter( "Application" )
{}

//____________________________________________
bool Application::realizeWidget()
{
    Debug::Throw( "Application::realizeWidget.\n" );

    // parse user argument
    const CommandLineParser parser( commandLineParser( _arguments() ) );
    if( parser.hasOption( "--user" ) )
    {
        XmlOptions::get().clearSpecialOptions( "USER_NAME" );
        XmlOptions::get().add( "USER_NAME", Option( parser.option( "--user" ), Option::Flag::Recordable|Option::Flag::Current  ) );
    }

    // check if the method has already been called.
    if( !BaseApplication::realizeWidget() ) return false;

    // interface
    interface_.reset( new JobInterface );

    // top widget
    mainWindow_ = new MainWindow;
    mainWindow_->setWindowTitle( "Top" );

    // connection between job manager and interface
    connect( mainWindow_, SIGNAL(commandRequested(JobCommand)), interface_.get(), SLOT(addCommand(JobCommand)) );
    connect( interface_.get(), SIGNAL(jobListReady(Job::Set)), mainWindow_, SLOT(processJobList(Job::Set)) );
    connect( interface_.get(), SIGNAL(userListReady(UserSet)), mainWindow_, SLOT(updateUsers(UserSet)) );
    connect( interface_.get(), SIGNAL(messageAvailable(QString)), &mainWindow_->statusBar().label(), SLOT(setTextAndUpdate(QString)) );

    mainWindow_->installFrames();
    mainWindow_->requestJobList();

    // configuration
    emit configurationChanged();

    // top widget
    mainWindow_->centerOnDesktop();
    mainWindow_->show();

    Debug::Throw( "Application::realizeWidget - done.\n" );
    return true;

}

//____________________________________________
CommandLineParser Application::commandLineParser( CommandLineArguments arguments, bool ignoreWarnings ) const
{
    Debug::Throw( "Application::commandLineParser.\n" );
    CommandLineParser out( BaseApplication::commandLineParser() );
    out.setGroup( CommandLineParser::applicationGroupName );
    out.registerOption( CommandLineParser::Tag( "--user", "-u" ), tr( "string" ), tr( "user name from whom the process list will be shown" ) );
    if( !arguments.isEmpty() ) out.parse( arguments, ignoreWarnings );
    return out;

}

//____________________________________________
void Application::usage() const
{
    _usage( "Top" );
    commandLineParser().usage();
    return;
}

//_______________________________________________
void Application::_configuration()
{

    Debug::Throw( "Application::_configuration.\n" );
    emit saveConfiguration();
    ConfigurationDialog dialog;
    connect( &dialog, SIGNAL(configurationChanged()), SIGNAL(configurationChanged()) );
    dialog.centerOnWidget( qApp->activeWindow() );
    dialog.exec();

}

//________________________________________________
bool Application::_processCommand( Server::ServerCommand command )
{

    Debug::Throw( "Application::_processCommand.\n" );
    if( BaseApplication::_processCommand( command ) ) return true;
    if( command.command() == Server::ServerCommand::Raise )
    {
        CommandLineParser parser( commandLineParser( command.arguments() ) );
        if( parser.hasOption( "--user" ) )
        { mainWindow_->addFrame( parser.option( "--user" ) ); }
        mainWindow_->uniconify();
        return true;

    } else return false;

}
