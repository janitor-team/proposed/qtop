#ifndef JobInformationDialog_h
#define JobInformationDialog_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "CustomDialog.h"
#include "Job.h"
#include "JobRecord.h"
#include "Key.h"
#include "RecordHistogram.h"

#include <QCloseEvent>
#include <QLabel>
#include <QResizeEvent>
#include <QTabWidget>

class GridLayoutItem;

class JobInformationDialog: public CustomDialog, public Base::Key
{

    Q_OBJECT

    public:

    //* constructor
    explicit JobInformationDialog( QWidget* );

    //*@name accessors
    //@{

    //* job
    const Job& job() const
    { return job_; }

    //* record
    const JobRecord& record() const
    { return record_; }

    //@}

    //*@name modifiers
    //@{

    //* job
    void setJob( const Job&, const JobRecord& );

    //@}

    Q_SIGNALS:

    //* emitted when dialog is closed
    void closed( JobRecord );

    public Q_SLOTS:

    //* update from job set
    void update( Job::Set );

    protected:

    //* resize event
    void closeEvent( QCloseEvent* ) override;

    //* resize event
    void resizeEvent( QResizeEvent* ) override;

    private Q_SLOTS:

    //* configuration
    void _updateConfiguration();

    private:

    //* update job information
    void _updateJobInformation();

    //* update from record
    void _updateHistograms();

    //* job
    Job job_;

    //* record
    JobRecord record_;

    //* true if associate job is alive
    bool alive_ = true;

    //* tab widget
    QTabWidget* tabWidget_ = nullptr;

    //* icon label
    QLabel* iconLabel_ = nullptr;

    //* name
    GridLayoutItem* nameLabel_ = nullptr;

    //* id
    GridLayoutItem* idLabel_ = nullptr;

    //* parentId
    GridLayoutItem* parentIdLabel_ = nullptr;

    //* user
    GridLayoutItem* userLabel_ = nullptr;

    //* command
    GridLayoutItem* commandLabel_ = nullptr;

    //* start time
    GridLayoutItem* startTimeLabel_ = nullptr;

    //* start time
    GridLayoutItem* completionTimeLabel_ = nullptr;

    //* Histogram bar
    RecordHistogram* cpuHistogram_ = nullptr;

    //* Histogram bar
    RecordHistogram* memoryHistogram_ = nullptr;

};

#endif
