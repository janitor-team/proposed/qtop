# $Id$
########### Qt configuration #########
if(USE_QT5)

  find_package(Qt5Widgets REQUIRED)
  find_package(Qt5Network REQUIRED)
  find_package(Qt5Xml REQUIRED)

else()

  find_package(Qt4 REQUIRED)
  set(QT_USE_QTXML TRUE)
  set(QT_USE_QTNETWORK TRUE)
  include(${QT_USE_FILE})
  add_definitions(${QT_DEFINITIONS})

endif()

########### X11 #########
if(UNIX AND NOT APPLE)

  find_package(XCB COMPONENTS XCB)
  if( XCB_XCB_FOUND )
    add_definitions(-DHAVE_XCB=1)
  else()
    add_definitions(-DHAVE_XCB=0)
  endif()

endif()

########### includes #########
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/base)
include_directories(${CMAKE_SOURCE_DIR}/base-qt)
include_directories(${CMAKE_SOURCE_DIR}/base-server)

########### configuration files ###############
configure_file(Config.h.cmake Config.h)

########### next target ###############
set(Top_SOURCES
  Application.cpp
  ConfigurationDialog.cpp
  HistogramWidget.cpp
  Job.cpp
  JobCommand.cpp
  JobInformationDialog.cpp
  JobInterface.cpp
  JobManager.cpp
  JobManagerFrame.cpp
  JobModel.cpp
  JobRecord.cpp
  JobThread.cpp
  MainWindow.cpp
  Menu.cpp
  NewFrameDialog.cpp
  RecordHistogram.cpp
  SignalMenu.cpp
  SignalJobsDialog.cpp
  SummaryFrame.cpp
  ToolTipWidget.cpp
  UserSet.cpp
  XcbInterface.cpp
  main.cpp
)

set(Top_RESOURCES pixmaps.qrc)

if(USE_QT5)

  qt5_add_resources(Top_RESOURCES_RCC ${Top_RESOURCES})

else()

  qt4_add_resources(Top_RESOURCES_RCC ${Top_RESOURCES})

endif()

add_application_icon(Top_SOURCES ${CMAKE_SOURCE_DIR}/Top)
add_desktop_file(${CMAKE_SOURCE_DIR}/Top)
ADD_UNIX_EXECUTABLE(Top
  ${Top_SOURCES}
  ${Top_RESOURCES_RCC}
)

target_link_libraries(Top ${XCB_LIBRARIES})
target_link_libraries(Top ${QT_LIBRARIES})
target_link_libraries(Top base base-qt base-server)

install(TARGETS Top DESTINATION ${BIN_INSTALL_DIR})

if(USE_QT5)

  target_link_libraries(Top Qt5::Network Qt5::Widgets Qt5::Xml)

endif()
