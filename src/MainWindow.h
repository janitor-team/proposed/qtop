#ifndef MainWindow_h
#define MainWindow_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "BaseMainWindow.h"
#include "Counter.h"
#include "CustomToolBar.h"
#include "Debug.h"
#include "JobCommand.h"
#include "Job.h"
#include "JobRecord.h"
#include "Key.h"
#include "UserSet.h"

#include <QCloseEvent>

class TabWidget;
class JobManagerFrame;
class BaseStatusBar;
class SummaryFrame;
class SignalMenu;
class UserSelectionFrame;

class MainWindow: public BaseMainWindow, public Base::Key
{

    //* Qt meta object declaration
    Q_OBJECT

    public:

    //* constructor
    explicit MainWindow( QWidget* = nullptr );

    //* user
    UserSelectionFrame& userSelectionFrame() const
    { return *userFrame_; }

    //* toolbar
    CustomToolBar& userToolBar()
    { return *userToolBar_; }

    //* toolbar
    CustomToolBar& toolBar()
    { return *toolbar_; }

    //* active frame
    bool hasActiveFrame() const
    { return (bool) activeFrame_; }

    //* active frame
    JobManagerFrame& activeFrame() const
    { return *activeFrame_; }

    //* status bar
    BaseStatusBar& statusBar() const
    { return *statusBar_; }

    //* pause state
    bool isPaused() const
    { return isPaused_; }

    //* install frames
    void installFrames();

    //*@name actions
    //@{

    //* toggle summary
    QAction& toggleSummaryAction() const
    { return *toggleSummaryAction_; }

    //* resume paused job
    QAction& resumeAction() const
    { return *resumeAction_; }

    //* pause job
    QAction& pauseAction() const
    { return *pauseAction_; }

    //* kill job
    QAction& killAction() const
    { return *killAction_; }

    //* update
    QAction& updateAction() const
    { return *updateAction_; }

    //* tree view action
    QAction& treeViewAction() const
    { return *treeViewAction_; }

    //* lock
    QAction& lockAction() const
    { return *lockAction_; }

    //* add frame
    QAction& addFrameAction() const
    { return *addFrameAction_; }

    //* remove current frame
    QAction& removeFrameAction() const
    { return *removeFrameAction_; }

    //* record
    QAction& recordAction() const
    { return *recordAction_; }

    //* record
    QAction& toggleRecordAction() const
    { return *toggleRecordAction_; }

    //@}

    Q_SIGNALS:

    //* request command
    void commandRequested( JobCommand );

    //* new jobs available
    void newJobs( Job::Set );

    public Q_SLOTS:

    //* update actions visibility
    void updateActions();

    //* update users
    void updateUsers(UserSet);

    //* add frame
    void addFrame( const QString& );

    //* run top to look for job updates
    void requestJobList();

    //* process job list
    void processJobList( Job::Set );

    //* close record dialogs
    void closeJobInformationDialogs();

    protected Q_SLOTS:

    //* update configuration
    void _updateConfiguration();

    //* save configuration
    void _saveConfiguration();

    //* update uesr
    void _updateUser( QString );

    //* active frame changed
    void _activeFrameChanged( int );

    //* toggle summary
    void _toggleSummary( bool );

    //* resume selected jobs
    void _resume();

    //* pause selected jobs
    void _pause();

    //* kill selected jobs
    void _kill();

    //* send signal to selected jobs
    void _sendSignal( int );

    //* opens recorder for selected jobs
    void _record();

    //* toggle record all jobs
    void _toggleRecordAllJobs( bool );

    //* tree view
    void _toggleTreeView( bool );

    //* pause/restart timer
    void _toggleUpdate( bool );

    //* add frame
    void _addFrame();

    //* remove frame
    void _removeFrame();

    //* remove frame at index
    void _removeFrame( int );

    //* record dialog closed
    void _recordDialogClosed( JobRecord );

    protected:

    //* timer event
    void timerEvent( QTimerEvent* );

    //* close window event handler
    void closeEvent( QCloseEvent* );

    //* active frame
    void _setActiveFrame( JobManagerFrame& frame );

    //* create a new frame
    JobManagerFrame& _newFrame();

    //* update frames
    void _updateFrames( const Job::Set& );

    //* update job records
    void _updateRecords( Job::Set );

    private:

    //* summary
    void _installActions();

    //* used to find records which have no matching job
    /**
    stores a set of jobs. For each jobRecord, loop for a matching
    job in the set. If one is found, the record gets updated and the
    job is erased from the set. False is returned. If none is found
    true is returned. Since the set of jobs is passed as a reference,
    the set of jobs is automatically updated. Remaining jobs are to be added to the list
    */
    class NoJobFTor
    {

        public:

        //* constructor
        explicit NoJobFTor( Job::Set& jobs ):
            jobs_( jobs )
        {}

        //* predicate
        /**
        stores a set of jobs. For each jobRecord, loop for a matching
        job in the set. If one is found, the record gets updated and the
        job is erased from the set. False is returned. If none is found
        true is returned.
        */
        bool operator () ( JobRecord& );

        private:

        // reference set of jobs
        Job::Set& jobs_;

    };

    //* toolbar
    CustomToolBar* userToolBar_ = nullptr;

    //* toolbar
    CustomToolBar* toolbar_ = nullptr;

    //* user
    UserSelectionFrame* userFrame_ = nullptr;

    //* status bar
    BaseStatusBar* statusBar_ = nullptr;

    //* tab widget
    TabWidget* tabWidget_ = nullptr;

    //* active frame
    JobManagerFrame* activeFrame_ = nullptr;

    //* signal menu
    SignalMenu* signalMenu_ = nullptr;

    //* true if paused
    bool isPaused_ = false;

    //* refresh rate
    int refreshRate_ = 0;

    //* timer object for scheduled update
    QBasicTimer timer_;

    //* local copy of jobs
    Job::Set jobs_;

    //* list of JobRecord associated to found jobs.
    /** it is used to create histograms of a given job resources */
    JobRecord::List records_;

    //*@name actions
    //@{

    //* resume paused job
    QAction* resumeAction_ = nullptr;

    //* pause job
    QAction* pauseAction_ = nullptr;

    //* kill job
    QAction* killAction_ = nullptr;

    //* record
    QAction* recordAction_ = nullptr;

    //* record all jobs flag
    QAction* toggleRecordAction_ = nullptr;

    //* update
    QAction* updateAction_ = nullptr;

    //* tree view
    QAction* treeViewAction_ = nullptr;

    //* lock
    QAction* lockAction_ = nullptr;

    //* toggle summary frame
    QAction* toggleSummaryAction_ = nullptr;

    //* add frame
    QAction* addFrameAction_ = nullptr;

    //* remove frame
    QAction* removeFrameAction_ = nullptr;

    //@}

};

#endif
