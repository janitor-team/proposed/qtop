/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "JobCommand.h"
#include "CppUtil.h"

//___________________________________________________________________
const JobCommand::NameMap& JobCommand::_names()
{
    static const NameMap names = Base::makeT<NameMap>(
    {
        { JobCommand::Type::None, QObject::tr( "None" ) },
        { JobCommand::Type::JobList, QObject::tr( "Query process list" ) },
        { JobCommand::Type::Signal, QObject::tr( "Signal selected processes" ) }
    });

    return names;
}

//___________________________________________________________________
int& JobCommand::_counter()
{
    static int counter( 0 );
    return counter;
}
