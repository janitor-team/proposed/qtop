/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "SignalMenu.h"

#include "Debug.h"
#include "IconEngine.h"
#include "IconNames.h"

#include <QActionGroup>
#include <sys/types.h>
#include <signal.h>

//________________________________________________________________
SignalMenu::SignalMenu( QWidget* parent ):
    QMenu( parent ),
    Counter( "SignalMenu" )
{
    Debug::Throw( "SignalMenu::SignalMenu.\n" );

    setTitle( tr( "Send Signal" ) );

    // action group
    QActionGroup* group = new QActionGroup( this );
    group->setExclusive( false );
    connect( group, SIGNAL(triggered(QAction*)), SLOT(_requestSignal(QAction*)) );

    // create actions
    QAction* action;
    addAction( action = new QAction( IconEngine::get( IconNames::Pause ), signalName(SIGSTOP), this ) );
    actions_.insert( action, SIGSTOP );
    group->addAction( action );

    addAction( action = new QAction( IconEngine::get( IconNames::Resume ), signalName(SIGCONT), this ) );
    actions_.insert( action, SIGCONT );
    group->addAction( action );

    addAction( action = new QAction( signalName(SIGINT), this ) );
    actions_.insert( action, SIGINT );
    group->addAction( action );

    addAction( action = new QAction( signalName(SIGTERM), this ) );
    actions_.insert( action, SIGTERM );
    group->addAction( action );

    addAction( action = new QAction( IconEngine::get( IconNames::Kill ), signalName(SIGKILL), this ) );
    actions_.insert( action, SIGKILL );
    group->addAction( action );

}

//________________________________________________________________
QString SignalMenu::signalName( int signal ) const
{
    Debug::Throw( "SignalMenu:signalName.\n" );
    switch( signal )
    {
        case SIGSTOP: return tr( "Suspend" );
        case SIGCONT: return tr( "Resume" );
        case SIGINT: return tr( "Interrupt" );
        case SIGTERM: return tr( "Terminate" );
        case SIGKILL: return tr( "Kill" );
        default: return tr( "Unknown Signal" );
    }

}

//________________________________________________________________
void SignalMenu::_requestSignal( QAction* action )
{
    Debug::Throw( "SignalMenu:_requestSignal.\n" );
    ActionMap::const_iterator iter( actions_.find( action ) );
    if( iter != actions_.end() ) emit signalRequested( iter.value() );
}
