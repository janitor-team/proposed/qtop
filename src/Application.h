#ifndef Application_h
#define Application_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "BaseApplication.h"
#include "Config.h"
#include "CommandLineArguments.h"
#include "Counter.h"
#include "IconEngine.h"
#include "JobInterface.h"

#include <memory>

class MainWindow;
// class JobInterface;

//* Main Window singleton object
class Application: public BaseApplication, private Base::Counter<Application>
{

    //* Qt meta object declaration
    Q_OBJECT

    public:

    //* constructor
    explicit Application( CommandLineArguments );

    //* create all widgets
    bool realizeWidget() override;

    //* interface
    JobInterface& interface() const
    { return *interface_; }

    //* retrieves top layout
    MainWindow& mainWindow() const
    { return *mainWindow_; }

    //*@name application information
    //@{

    //* command line parser
    CommandLineParser commandLineParser( CommandLineArguments = CommandLineArguments(), bool ignoreWarnings = true ) const override;

    //* usage
    void usage() const override;

    //* application name
    QString applicationName() const override
    { return "Top"; }

    //* application icon
    QIcon applicationIcon() const override
    { return IconEngine::get( ":/Top.png" ); }

    // application version
    QString applicationVersion() const override
    { return VERSION; }

    //@}

    protected Q_SLOTS:

    //* configuration
    void _configuration() override;

    //* process request from application manager
    bool _processCommand( Server::ServerCommand ) override;

    private:

    //* pointer to top interface
    std::unique_ptr<JobInterface> interface_;

    //* pointer to top widget
    MainWindow *mainWindow_ = nullptr;

};

#endif
