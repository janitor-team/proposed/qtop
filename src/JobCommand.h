#ifndef JobCommand_h
#define JobCommand_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"
#include "CppUtil.h"
#include "Debug.h"

#include <QString>
#include <QStringList>
#include <QList>
#include <QHash>

//* handles top commands
class JobCommand final: private Base::Counter<JobCommand>
{

    public:

    //* enumeration for command type
    enum class Type
    {
        None,
        JobList,
        Signal
    };

    //* list of commands
    using List = QList<JobCommand>;

    //* constructor
    explicit JobCommand( const Type type = Type::None ):
        Counter( "JobCommand" ),
        id_( _counter()++ ),
        type_( type )
    {}

    //* command name
    QString name() const
    { return name( type_ ); }

    //* map Type to command name
    static QString name( Type type )
    { return _names()[type]; }

    //* id
    int id() const
    { return id_; }

    //* type
    Type type() const
    { return type_; }

    //*@name interface to arguments
    //@{

    //* add argument
    JobCommand& addArgument( const QString& argument )
    {
        arguments_ << argument;
        return *this;
    }

    //* push arguments
    JobCommand& operator << (const QString& argument )
    { return addArgument( argument ); }

    //* arguments
    const QStringList& arguments() const
    { return arguments_; }

    //* clear arguments
    void clearArguments()
    { arguments_.clear(); }

    //@}

    private:

    //* static id counter
    static int& _counter();

    //* map types to command name
    using NameMap = QHash<Type,QString>;
    static const NameMap& _names();

    //* unique id
    int id_;

    //* type
    Type type_ = Type::None;

    //* arguments
    QStringList arguments_;

};

//* less than operator
inline bool operator < ( const JobCommand& first, const JobCommand& second)
{ return first.id() < second.id(); }

//* equal to operator
inline bool operator == (const JobCommand& first, const JobCommand& second)
{ return first.id() == second.id(); }

#endif
