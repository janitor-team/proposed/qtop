#ifndef JobModel_h
#define JobModel_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"
#include "Job.h"
#include "TreeModel.h"

#include <QColor>

//* Job model. Stores job information for display in lists
class JobModel : public TreeModel<Job>, private Base::Counter<JobModel>
{

    //* Qt meta object declaration
    Q_OBJECT

    public:

    //* constructor
    explicit JobModel(QObject *parent = nullptr);

    //* column type enumeration
    enum ColumnType
    {
        Name,
        User,
        Id,
        ParentId,
        Cpu,
        Memory,
        CpuTime,
        Priority,
        Nice,
        VirtualMemory,
        ResidentMemory,
        SharedMemory,
        Threads,
        StartTime,
        Command,
        Empty,
        nColumns
    };

    //*@name methods reimplemented from base class
    //@{

    // return job for a given index
    QVariant data( const QModelIndex&, int ) const override;

    //* header data
    QVariant headerData( int, Qt::Orientation, int = Qt::DisplayRole ) const override;

    //* number of columns for a given index
    int columnCount(const QModelIndex& = QModelIndex()) const override
    { return nColumns; }

    //@}

    //* today
    void setToday( const TimeStamp& today )
    { today_ = today; }

    protected:

    //* sorting
    void _sort( int column, Qt::SortOrder order ) override
    { _root().sort( SortFTor( column, order ) ); }

    private Q_SLOTS:

    //* configuration
    void _updateConfiguration();

    private:


    //* used to sort IconCaches
    class SortFTor: public ItemModel::SortFTor
    {

        public:

        //* constructor
        explicit SortFTor( int type, Qt::SortOrder order ):
            ItemModel::SortFTor( type, order )
        {}

        //* prediction
        bool operator() ( const Item& first, const Item& second ) const
        { return (*this)( first.get(), second.get() ); }

        //* prediction
        bool operator() ( Job, Job ) const;

    };

    //* today
    TimeStamp today_;

    //* true if icons must be shown
    bool showIcons_;

    //* icon size
    QSize iconSize_;

    //* empty icon
    QIcon emptyIcon_;

};

#endif
